import React, {useState, useEffect} from 'react'
// import Form from 'react-bootstrap/Form';
// import Button from 'react-bootstrap/Button';
import AppHelper from '../app-helper';

export default function totalBalance() {


const[money, setMoney] = useState([])

useEffect(()=>{

    const netIncome =async() =>{

        const response = await fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`,{
            headers:{
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        })
        const data = await response.json()
        console.log(data)

        setMoney(data.totalMoney)
    }
    netIncome()
},[])


		return(
            <div>
                <h1>{money}</h1>
                <h6>(Total Balance)</h6>
            </div>

			)
}