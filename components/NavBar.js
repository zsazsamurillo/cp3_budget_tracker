import React, { useContext } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';


export default function NavBar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar className="navbar navbar-expand=lg " bg="light" >
            <Link href="/expense-tracker">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>

            <Link href="/graph">
                <a className="nav-link">Graph</a>
            </Link>

            <Link href="/allTransactions">
                <a className="nav-link">Transactions</a>
            </Link> 

            <Link href="/">
                <a className="nav-link">Logout</a>
            </Link>
            


        </Navbar>
    )
}
