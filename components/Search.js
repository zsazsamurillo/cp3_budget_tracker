import React, {useState} from 'react'

import {Button, Form, Table} from 'react-bootstrap'; 
import AppHelper from '../app-helper'

export default function Forms() {

    const [search, setSearch] = useState('')
    const [newsearch, setnewSearch] = useState('')
    const [display, setDisplay] = useState('')

    function searchRecord(e){
        e.preventDefault()

        fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`,{
            headers:{
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        }).then(res=>res.json())
        .then(data =>{
            // console.log(data)

           let searchArray= data.records.map(i=>{
                if(search == i.categoryName){
                    let transactionDate=new Date(i.transactionDate)
                    return(

                        <Table striped bordered hover size="sm">
       
            <tbody>
                     <tr key={i._id}>
                      <td className ="text-left ">{i.amount}</td>
                      <td className ="text-left ">{i.categoryName}</td>
                      <td className ="text-left ">{i.categoryType}</td>
                      <td className ="text-left">{i.description}</td>
                      <td className ="text-left">{transactionDate.toDateString()}</td>
                    </tr>
                    </tbody>
          </Table>
                        )
                }

            })
          
           setnewSearch(searchArray)
        })

        console.log(newsearch)
        
    }

    return (

        <>
       <Form onSubmit={e => searchRecord(e)} className="col-lg-4 offset-lg-4 my-5">
            <Form.Group>

            <Form.Label>Search</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Search...."
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                    required
                />
               <Button 
                    className="bg-primary"
                    type="submit"
                    id="submitBtn"
                >
                    Search
                </Button>
    </Form.Group>
        </Form>
        
        <ul>
            {newsearch}

        </ul>
        </>
        )

}
