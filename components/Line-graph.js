import React,{useState,useEffect} from 'react';
import {Line} from 'react-chartjs-2';
import AppHelper from '../app-helper'

export default function index(){

	const [display, setDisplay] =useState([])
  const [displayLabel, setDisplayLabel] =useState([])



	useEffect(async () => { 

		const response = await fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`,{
			headers:{
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			}
			})

			const data = await response.json()

      let lineArray = []
      data.records.map(i=>{
        lineArray.push(i.currentMoney)
      })

      let lineArrayLabel = []
      lineArray.map(index=>{
        lineArrayLabel.push('*')
      })
      console.log(lineArray)
      setDisplay(lineArray)
      console.log(display)
      setDisplayLabel(lineArrayLabel)

			

	},[])
    return(
    	 
       <Line
                data={{
                    datasets: [{
                        label: 'Balance Trend',
                        data: display,
                        backgroundColor: ['#fee715ff']
                    }],
                    labels: displayLabel
                    
                }} 
                />
      

    )
}