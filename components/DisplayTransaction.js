import React, {useState, useEffect} from 'react'
// import Form from 'react-bootstrap/Form';
// import Button from 'react-bootstrap/Button';
import AppHelper from '../app-helper';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table} from 'react-bootstrap';

export default function display_trans() {


const[records, setRecords] = useState([])

useEffect(()=>{

	const transactionHistory =async() =>{

		const response = await fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`,{
			headers:{
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			}
		})
		const data = await response.json()
		console.log(data)

		setRecords(data.records.reverse())
	}
	transactionHistory()
},[])

	return(

		<div>
				<Table striped bordered hover size="sm">
         <thead>
		    <tr>
		      <th>Amount</th>
		      <th>Income or Expenses</th>
		      <th>Inc or Exp Type</th>
		      <th>Description</th>
		      <th> TransactionDate </th>
		    </tr>
		  </thead>
		  
			{records.map(record =>{

				let transactionDate = new Date(record.transactionDate)

				return(
		  
					<tbody>
					 <tr key={record._id}>
				      <td className ="text-left p-3">{record.amount}</td>
				      <td>{record.categoryName}</td>
				      <td>{record.categoryType}</td>
				      <td>{record.description}</td>
				      <td>{transactionDate.toDateString()}</td>
				    </tr>
				    </tbody>
				    
						
					)
			})}

			</Table>
		</div>


		)

			}
		
	