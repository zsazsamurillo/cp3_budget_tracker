import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import AppHelper from '../app-helper';
import Router from 'next/router';
import { Row, Col } from 'react-bootstrap';

export default function add_create() {
    //declare form input states
    const [text, setText] = useState('');
    const [amount, setAmount] = useState('');
    const [choice, setChoice] = useState('');
    const [misc, setMisc] = useState('');

    //function for processing creation of a new course
    function createTransaction(e) {
        e.preventDefault();

        console.log(`${text}`);

       

        fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/addRecord`,{
            method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${AppHelper.getAccessToken()}`
                },
                body: JSON.stringify({
                    "categoryType": misc,
                    "categoryName": choice,
                    "description": text ,
                    "amount": amount
                })
        }).then(res => {
                return res.text()
            }).then(data => {
                console.log(data)
                
            })      
            Router.push('/allTransactions')
            
       
    }

    return (

        <Form onSubmit={(e) => createTransaction(e)}>


            <Row>
            <Col sm={6}><Form.Group controlId="choice">
                <Form.Label>Income / Expense</Form.Label>
                <Form.Control as="select" onChange={e=> {
                        let i=e.target.value 
                        setChoice(i)      
                }}>
                    <option value="select">-----</option>
                  <option value="Income" >Income</option>
                  <option value="Expense">Expense</option>
                </Form.Control>
              </Form.Group></Col>

            <Col sm={6}>
            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter Amount"
                    value={amount}
                    onChange={e => setAmount(e.target.value)}
                    required
                />
            </Form.Group>
            </Col>
          </Row>

            <Form.Group controlId="text">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Short description of the Transaction"
                    value={text}
                    onChange={e => setText(e.target.value)}
                    required
                />
            </Form.Group>

                        <Form.Group controlId="text">
                <Form.Label>Type</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="eg. bills / food / etc."
                    value={misc}
                    onChange={e => setMisc(e.target.value)}
                    required
                />
            </Form.Group>

            

            <Button className="bg-primary" type="submit">
                Add Transaction
            </Button>
        </Form>
    )
}

