import React from 'react';
 import NavBar from '../../components/NavBar';
import Search from '../../components/Search'

import Total from '../../components/Total'
import Amount from '../../components/Amount'

import { Row, Col} from 'react-bootstrap';

export default function index(){

    return(
        <>
        	<NavBar />
        	<Row>
        	<Col sm={6}>
        	<div>
            <Total/>
            <Amount/>
            </div>
            </Col>
            <Col sm={6}>
            <Search/>
            </Col>
            </Row>
        </>
    )
}