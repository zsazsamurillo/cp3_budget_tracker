import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import View from '../components/View';
import AppHelper from '../app-helper';


export default function index(){

	return(
		<View title={ 'Login' }>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
					<LoginForm />
				</Col>
			</Row>
		</View>
	)
}

const LoginForm = () => {

	
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	function authenticate(e){

		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		// "{ email: email, password: password }"

		fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			// console.log(data);
			console.log(data.accessToken
)
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Router.push('/expense-tracker')
			} else {

				if(data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
				} else if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }

			}

		})

		// const match = users.find(user => {
		// 	return (user.email === email && user.password === password)
		// })

		// if(match){
		// 	//set the email of the authenticated user in local storage
		// 	localStorage.setItem('email', email);

		// 	localStorage.setItem('isAdmin', match.isAdmin)

		// 	//set the global user state to have its email property be obtained from local storage
		// 	setUser({
		// 		email: localStorage.getItem('email'),
		// 		isAdmin: match.isAdmin
		// 	})

		// 	Router.push('/courses');

		// }else{
		// 	alert('Login failed. Please check your details')

		// 	setEmail("")
		// 	setPassword("")
		// }

	}

	const authenticateGoogleToken = (response) => {
	        
        console.log(response.tokenId)

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

                	fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/verify-google-id-token`,options).then(AppHelper.toJSON).then(data=>{
                		console.log(data)
        			if(typeof data.accessToken !== 'undefined'){
        				localStorage.setItem('token', data.accessToken)
        				retrieveUserDetails(data.accessToken)
        				
        			}else{
        				if(data.error === "google-auth-error"){
        					Swal.fire('Google Auth Error', 'Google Authentication Failed', 'error')
        				}else if(data.error === "login-type-error"){
        					Swal.fire('Login Type Error', 'You may have login to a different user login procedure', 'error')
        				}
        			}
        	})

    };



	const retrieveUserDetails = (accessToken) => {

		const options = {
			headers: { Authorization: `Bearer ${ accessToken }` }
		}

		fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			console.log(data)

			setUser({ id: data._id, isAdmin: data.isAdmin });
			Router.push('/expense-tracker');

		})

	}

	return(
	
		<React.Fragment>
			<Head>
				<title>Authentication</title>
			</Head>
			<Card
				bg = "light"
			>
			<Card.Body>
			<Container>
				<Form onSubmit={e => authenticate(e)}>

					<Form.Group controlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Email Address" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password">
		                <Form.Label>Password:</Form.Label>
		                <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		            </Form.Group>

		            <Button variant="primary" type="submit" block>Submit</Button>

		            <GoogleLogin 
		            	clientId="572311522502-le83vksl2pnmp7k6rjkkeht3e7qgjbsf.apps.googleusercontent.com"
		            	buttonText="Login"
		            	onSuccess={ authenticateGoogleToken }
		            	onFailure={ authenticateGoogleToken }
		            	cookiePolicy={ 'single_host_origin' }
		            	className="w-100 text-center d-flex justify-content-center"
		            />
		            <p>No Account yet?<a href="/register"> Create an account</a></p>

				</Form>
			</Container>
			</Card.Body>
			</Card>
		</React.Fragment>
	)
}

