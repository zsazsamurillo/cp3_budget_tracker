import  React,{ useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';

export default function index(){
	const { unsetUser, setUser } = useContext(UserContext);

	useEffect(() => {
		//invoke unsetUser to clear local storage
		unsetUser();
		Router.push('/');
	}, [])

	return null;
}