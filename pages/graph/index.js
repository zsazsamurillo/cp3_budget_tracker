import React,{useState,useEffect} from 'react';
import {Line, Doughnut} from 'react-chartjs-2';
import AppHelper from '../../app-helper'
import NavBar from '../../components/NavBar'
import LineGraph from '../../components/Line-graph'
import { Row, Col } from 'react-bootstrap';

export default function index(){

	const [display, setDisplay] =useState([])
  const [totalIncome ,setTotalIncome] = useState([])
  const [totalExpense ,setTotalExpense] = useState([])
  const [TotalInc, setTotalInc] = useState(0)
  const [TotalExp, setTotalExp] = useState(0)
	const recordArray = []
  const incomeArray =[]
  const expenseArray =[]
  let inc = 0, exp=0
	useEffect(async () => { 

		const response = await fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/details`,{
			headers:{
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			}
			})

			const data = await response.json()

			data.records.map(i=>{
				recordArray.push(i.currentMoney)
			})

      data.records.map(i=>{
        i.categoryName == `Income` ? incomeArray.push(i.amount) : expenseArray.push(i.amount)


      })
      // console.log(incomeArray)
      incomeArray.map(i=>{
        inc += i
      })
      console.log(inc)

      setTotalInc(inc)


			setDisplay(recordArray)

        expenseArray.map(i=>{
        exp += i
      })
      console.log(exp)
      setTotalExp(exp)


	},[])
    return(
      <>
        <NavBar/>
        <Row>
    
        <Col sm={6}><Doughnut
                data={{
                    datasets: [{
                        label: 'Balance Trend',
                        data: [TotalInc, TotalExp],
                        backgroundColor: ['#fee715ff', 'red']
                    }],
                    labels: ['Income', 'Expense']
                    
                }}
                /></Col>

        <Col sm={6}><LineGraph/></Col>
     </Row>
    
    	 
      </>

    )
}