import React,{ useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import AppHelper from '../../app-helper'


export default function index() {
	const [email, setEmail] = useState('');

	const [firstname,setfirstname] = useState('');
	const [lastname, setlastname] = useState('');

	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {
		e.preventDefault();
			const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstname,
				lastName: lastname,
				email: email,
				password: password1
			})
		}

		// "{ email: email, password: password }"

		fetch(`https://guarded-hollows-97125.herokuapp.com/api/users/register`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)

		})



		setEmail('');
		setPassword1('');
		setPassword2('');
		
		

		console.log('Thank you for registering!');
		Router.push('/')
		
	}
	// form => registerUser()

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2]);

	console.log(email)
	console.log(password1)


	return (
		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>

			<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter first name"
					value={firstname}
					onChange={e => setfirstname(e.target.value)}
					required
				/>


				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					value={lastname}
					onChange={e => setlastname(e.target.value)}
					required
				/>


				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive
				?
				<Button 
					className="bg-primary"
					type="submit"
					id="submitBtn"
				>
					Submit
				</Button>
				:
				<Button 
					className="bg-danger"
					type="submit"
					id="submitBtn"
					disabled
				>
					Submit
				</Button>
			}
			 <p>Already haave an account?<a href="/login"> Login Here</a></p>
	
		</Form>
	)
};
